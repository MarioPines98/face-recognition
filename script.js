import { tinyFaceDetector } from "/models/tinyFaceDetector/tinyFaceDetector.ts"


// Notice that recognizer is going to be launch in a camera
const video = document.getElementById('video');

// I need to take all modules to detect face and expressions in order to record
Promise.all([
    tinyFaceDetector,
    faceapi.nets.faceLandmark68Net.load('/models/faceLandmarkNet/faceLandmark68Net.ts'),
    /*faceapi.nets.faceRecognitionNet.loadFromUri('/models/'),
    faceapi.nets.faceExpressionNet.loadFromUri('/models'),*/
]).then(startVideo)

// At this point it is important to make sure i am recording
function startVideo(){
    navigator.getUserMedia(
        { video: {} },
        stream => video.srcObject = stream,
        err => console.error(err)
    )
}

// The last thing i need to make is add an event listener to my video
video.addEventListener('play',() => {
    const canvas = faceapi.createCanvasFromMedia(video)
    document.body.append(canvas)
    const displaySize = { width: video.width, height: video.height }
    faceapi.matchDimensions(canvas, displaySize)
    setInterval(async () => {
        const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions()
        const resizedDetections = faceapi.resizeResults(detections, displaySize)
        canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
        faceapi.draw.drawDetections(canvas, resizedDetections)
        faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
        faceapi.draw.drawFaceExpressions(canvas, resizedDetections)
      }, 100)
});

